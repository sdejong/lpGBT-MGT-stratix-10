module TX_RESET (
		input  wire       clock,                //                clock.clk
		input  wire [0:0] pll_cal_busy,         //         pll_cal_busy.pll_cal_busy
		input  wire [0:0] pll_locked,           //           pll_locked.pll_locked
		input  wire [0:0] pll_select,           //           pll_select.pll_select
		input  wire       reset,                //                reset.reset
		output wire [0:0] tx_analogreset,       //       tx_analogreset.tx_analogreset
		input  wire [0:0] tx_analogreset_stat,  //  tx_analogreset_stat.tx_analogreset_stat
		input  wire [0:0] tx_cal_busy,          //          tx_cal_busy.tx_cal_busy
		output wire [0:0] tx_digitalreset,      //      tx_digitalreset.tx_digitalreset
		input  wire [0:0] tx_digitalreset_stat, // tx_digitalreset_stat.tx_digitalreset_stat
		output wire [0:0] tx_ready              //             tx_ready.tx_ready
	);
endmodule

