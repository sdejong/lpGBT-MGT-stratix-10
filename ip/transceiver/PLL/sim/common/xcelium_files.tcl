
namespace eval PLL {
  proc get_design_libraries {} {
    set libraries [dict create]
    dict set libraries altera_common_sv_packages         1
    dict set libraries altera_xcvr_cdr_pll_s10_htile_180 1
    dict set libraries PLL                               1
    return $libraries
  }
  
  proc get_memory_files {QSYS_SIMDIR} {
    set memory_files [list]
    return $memory_files
  }
  
  proc get_common_design_files {USER_DEFINED_COMPILE_OPTIONS USER_DEFINED_VERILOG_COMPILE_OPTIONS USER_DEFINED_VHDL_COMPILE_OPTIONS QSYS_SIMDIR} {
    set design_files [dict create]
    dict set design_files "altera_common_sv_packages::altera_xcvr_native_s10_functions_h" "xmvlog -sv $USER_DEFINED_VERILOG_COMPILE_OPTIONS $USER_DEFINED_COMPILE_OPTIONS  \"$QSYS_SIMDIR/../altera_xcvr_cdr_pll_s10_htile_180/sim/altera_xcvr_native_s10_functions_h.sv\"  -work altera_common_sv_packages -cdslib  ./cds_libs/altera_common_sv_packages.cds.lib"
    return $design_files
  }
  
  proc get_design_files {USER_DEFINED_COMPILE_OPTIONS USER_DEFINED_VERILOG_COMPILE_OPTIONS USER_DEFINED_VHDL_COMPILE_OPTIONS QSYS_SIMDIR} {
    set design_files [list]
    lappend design_files "xmvlog -sv $USER_DEFINED_VERILOG_COMPILE_OPTIONS $USER_DEFINED_COMPILE_OPTIONS  \"$QSYS_SIMDIR/../altera_xcvr_cdr_pll_s10_htile_180/sim/alt_xcvr_resync.sv\"  -work altera_xcvr_cdr_pll_s10_htile_180 -cdslib  ./cds_libs/altera_xcvr_cdr_pll_s10_htile_180.cds.lib"                              
    lappend design_files "xmvlog -sv $USER_DEFINED_VERILOG_COMPILE_OPTIONS $USER_DEFINED_COMPILE_OPTIONS  \"$QSYS_SIMDIR/../altera_xcvr_cdr_pll_s10_htile_180/sim/alt_xcvr_arbiter.sv\"  -work altera_xcvr_cdr_pll_s10_htile_180 -cdslib  ./cds_libs/altera_xcvr_cdr_pll_s10_htile_180.cds.lib"                             
    lappend design_files "xmvlog -sv $USER_DEFINED_VERILOG_COMPILE_OPTIONS $USER_DEFINED_COMPILE_OPTIONS  \"$QSYS_SIMDIR/../altera_xcvr_cdr_pll_s10_htile_180/sim/s10_avmm_h.sv\"  -work altera_xcvr_cdr_pll_s10_htile_180 -cdslib  ./cds_libs/altera_xcvr_cdr_pll_s10_htile_180.cds.lib"                                   
    lappend design_files "xmvlog -sv $USER_DEFINED_VERILOG_COMPILE_OPTIONS $USER_DEFINED_COMPILE_OPTIONS  \"$QSYS_SIMDIR/../altera_xcvr_cdr_pll_s10_htile_180/sim/alt_xcvr_pll_rcfg_arb.sv\"  -work altera_xcvr_cdr_pll_s10_htile_180 -cdslib  ./cds_libs/altera_xcvr_cdr_pll_s10_htile_180.cds.lib"                        
    lappend design_files "xmvlog -sv $USER_DEFINED_VERILOG_COMPILE_OPTIONS $USER_DEFINED_COMPILE_OPTIONS  \"$QSYS_SIMDIR/../altera_xcvr_cdr_pll_s10_htile_180/sim/alt_xcvr_pll_embedded_debug.sv\"  -work altera_xcvr_cdr_pll_s10_htile_180 -cdslib  ./cds_libs/altera_xcvr_cdr_pll_s10_htile_180.cds.lib"                  
    lappend design_files "xmvlog -sv $USER_DEFINED_VERILOG_COMPILE_OPTIONS $USER_DEFINED_COMPILE_OPTIONS  \"$QSYS_SIMDIR/../altera_xcvr_cdr_pll_s10_htile_180/sim/alt_xcvr_pll_avmm_csr.sv\"  -work altera_xcvr_cdr_pll_s10_htile_180 -cdslib  ./cds_libs/altera_xcvr_cdr_pll_s10_htile_180.cds.lib"                        
    lappend design_files "xmvlog -sv $USER_DEFINED_VERILOG_COMPILE_OPTIONS $USER_DEFINED_COMPILE_OPTIONS  \"$QSYS_SIMDIR/../altera_xcvr_cdr_pll_s10_htile_180/sim/PLL_altera_xcvr_cdr_pll_s10_htile_180_vzmxkfa.sv\"  -work altera_xcvr_cdr_pll_s10_htile_180 -cdslib  ./cds_libs/altera_xcvr_cdr_pll_s10_htile_180.cds.lib"
    lappend design_files "xmvlog -sv $USER_DEFINED_VERILOG_COMPILE_OPTIONS $USER_DEFINED_COMPILE_OPTIONS  \"$QSYS_SIMDIR/../altera_xcvr_cdr_pll_s10_htile_180/sim/alt_xcvr_pll_rcfg_opt_logic_vzmxkfa.sv\"  -work altera_xcvr_cdr_pll_s10_htile_180 -cdslib  ./cds_libs/altera_xcvr_cdr_pll_s10_htile_180.cds.lib"          
    lappend design_files "xmvhdl -v93 $USER_DEFINED_VHDL_COMPILE_OPTIONS $USER_DEFINED_COMPILE_OPTIONS  \"$QSYS_SIMDIR/PLL.vhd\"  -work PLL"                                                                                                                                                                                
    return $design_files
  }
  
  proc get_elab_options {SIMULATOR_TOOL_BITNESS} {
    set ELAB_OPTIONS ""
    if ![ string match "bit_64" $SIMULATOR_TOOL_BITNESS ] {
    } else {
    }
    return $ELAB_OPTIONS
  }
  
  
  proc get_sim_options {SIMULATOR_TOOL_BITNESS} {
    set SIM_OPTIONS ""
    if ![ string match "bit_64" $SIMULATOR_TOOL_BITNESS ] {
    } else {
    }
    return $SIM_OPTIONS
  }
  
  
  proc get_env_variables {SIMULATOR_TOOL_BITNESS} {
    set ENV_VARIABLES [dict create]
    set LD_LIBRARY_PATH [dict create]
    dict set ENV_VARIABLES "LD_LIBRARY_PATH" $LD_LIBRARY_PATH
    if ![ string match "bit_64" $SIMULATOR_TOOL_BITNESS ] {
    } else {
    }
    return $ENV_VARIABLES
  }
  
  
}
