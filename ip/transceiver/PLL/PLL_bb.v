module PLL (
		output wire  pll_cal_busy,  //  pll_cal_busy.pll_cal_busy
		output wire  pll_locked,    //    pll_locked.pll_locked
		input  wire  pll_refclk0,   //   pll_refclk0.clk
		output wire  tx_serial_clk  // tx_serial_clk.clk
	);
endmodule

