module PHY (
		input  wire [0:0]  rx_analogreset,          //          rx_analogreset.rx_analogreset
		output wire [0:0]  rx_analogreset_stat,     //     rx_analogreset_stat.rx_analogreset_stat
		input  wire [0:0]  rx_bitslip,              //              rx_bitslip.rx_bitslip
		output wire [0:0]  rx_cal_busy,             //             rx_cal_busy.rx_cal_busy
		input  wire        rx_cdr_refclk0,          //          rx_cdr_refclk0.clk
		output wire [0:0]  rx_clkout,               //               rx_clkout.clk
		input  wire [0:0]  rx_coreclkin,            //            rx_coreclkin.clk
		input  wire [0:0]  rx_digitalreset,         //         rx_digitalreset.rx_digitalreset
		output wire [0:0]  rx_digitalreset_stat,    //    rx_digitalreset_stat.rx_digitalreset_stat
		output wire [0:0]  rx_is_lockedtodata,      //      rx_is_lockedtodata.rx_is_lockedtodata
		output wire [0:0]  rx_is_lockedtoref,       //       rx_is_lockedtoref.rx_is_lockedtoref
		output wire [31:0] rx_parallel_data,        //        rx_parallel_data.rx_parallel_data
		input  wire [0:0]  rx_serial_data,          //          rx_serial_data.rx_serial_data
		input  wire [0:0]  tx_analogreset,          //          tx_analogreset.tx_analogreset
		output wire [0:0]  tx_analogreset_stat,     //     tx_analogreset_stat.tx_analogreset_stat
		output wire [0:0]  tx_cal_busy,             //             tx_cal_busy.tx_cal_busy
		output wire [0:0]  tx_clkout,               //               tx_clkout.clk
		input  wire [0:0]  tx_coreclkin,            //            tx_coreclkin.clk
		input  wire [0:0]  tx_digitalreset,         //         tx_digitalreset.tx_digitalreset
		output wire [0:0]  tx_digitalreset_stat,    //    tx_digitalreset_stat.tx_digitalreset_stat
		input  wire [31:0] tx_parallel_data,        //        tx_parallel_data.tx_parallel_data
		input  wire [0:0]  tx_serial_clk0,          //          tx_serial_clk0.clk
		output wire [0:0]  tx_serial_data,          //          tx_serial_data.tx_serial_data
		output wire [47:0] unused_rx_parallel_data, // unused_rx_parallel_data.unused_rx_parallel_data
		input  wire [47:0] unused_tx_parallel_data  // unused_tx_parallel_data.unused_tx_parallel_data
	);
endmodule

