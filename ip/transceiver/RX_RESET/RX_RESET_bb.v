module RX_RESET (
		input  wire       clock,                //                clock.clk
		input  wire       reset,                //                reset.reset
		output wire [0:0] rx_analogreset,       //       rx_analogreset.rx_analogreset
		input  wire [0:0] rx_analogreset_stat,  //  rx_analogreset_stat.rx_analogreset_stat
		input  wire [0:0] rx_cal_busy,          //          rx_cal_busy.rx_cal_busy
		output wire [0:0] rx_digitalreset,      //      rx_digitalreset.rx_digitalreset
		input  wire [0:0] rx_digitalreset_stat, // rx_digitalreset_stat.rx_digitalreset_stat
		input  wire [0:0] rx_is_lockedtodata,   //   rx_is_lockedtodata.rx_is_lockedtodata
		output wire [0:0] rx_ready              //             rx_ready.rx_ready
	);
endmodule

