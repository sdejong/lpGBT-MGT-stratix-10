# #
# # QSYS_SIMDIR is used in the Quartus-generated IP simulation script to
# # construct paths to the files required to simulate the IP in your Quartus
# # project. By default, the IP script assumes that you are launching the
# # simulator from the IP script location. If launching from another
# # location, set QSYS_SIMDIR to the output directory you specified when you
# # generated the IP script, relative to the directory from which you launch
# # the simulator.

# #
set QSYS_SIMDIR /home/srdejong/MGT
# #


# # Source the generated IP simulation script.
source $QSYS_SIMDIR/mentor/msim_setup.tcl
# #
# # Set any compilation options you require (this is unusual).
# set USER_DEFINED_COMPILE_OPTIONS <compilation options>
# set USER_DEFINED_VHDL_COMPILE_OPTIONS <compilation options for VHDL>
# set USER_DEFINED_VERILOG_COMPILE_OPTIONS <compilation options for Verilog>
# #
# # Call command to compile the Quartus EDA simulation library.
dev_com 
# #

# # Call command to compile the Quartus-generated IP simulation files.
com
# #
# # Add commands to compile all design files and testbench files, including
# # the top level. (These are all the files required for simulation other
# # than the files compiled by the Quartus-generated IP simulation script)
# #

vcom -reportprogress 300  -work work $QSYS_SIMDIR/MGT.vhd
vcom -reportprogress 300  -work work $QSYS_SIMDIR/MGT_testbench.vhd

set TOP_LEVEL_NAME MGT_testbench

set USER_DEFINED_ELAB_OPTIONS "-t ps"
elab
#add wave -position end sim:/mgt_testbench/MGT_inst/u0/*

proc tx_isReady {} {
    return [exa sim:/mgt_testbench/tx_ready]
}

proc tx_waitForReady {{timeout 100}} {

    for {set i 0} {$i < $timeout} {incr i} {
        run 1us
        
        if {[tx_isLocked] == 1} {
            return 0
        }
    }
    
    return -1
}

proc rx_isReady {} {
    return [exa sim:/mgt_testbench/rx_ready]
}

proc rx_waitForReady {{timeout 200}} {

    for {set i 0} {$i < $timeout} {incr i} {
        run 1us
        
        if {[rx_isLocked] == 1} {
            return 0
        }
    }
    
    return -1
}

proc bitSlip {} {
	force -deposit sim:/mgt_testbench/bitSlip 1 0
	run 300ns
	force -deposit sim:/mgt_testbench/bitSlip 0 0
	run 200ns
}

proc tx_reset {} {
	force -deposit sim:/mgt_testbench/tx_reset 1 0
	run 100ns
	force -deposit sim:/mgt_testbench/tx_reset 0 0
}

proc rx_reset {} {
	force -deposit sim:/mgt_testbench/rx_reset 1 0
	run 100ns
	force -deposit sim:/mgt_testbench/rx_reset 0 0
}


add wave -noupdate -divider -height 20 "Clocks"
add wave -position end -label "320MHz Clock" sim:/mgt_testbench/clk_mgt_320MHz_tx
add wave -position end -label "5.12GHz Pll Clock" sim:/mgt_testbench/MGT_inst/u0/pll/tx_serial_clk
add wave -position end -label "10.24GHz data clock" sim:/mgt_testbench/clk_serial_rx
add wave -position end -label "RX out clock" sim:/mgt_testbench/MGT_inst/u0/rx_clkout_clk(0)
add wave -position end -label "TX out clock" sim:/mgt_testbench/MGT_inst/u0/tx_clkout_clk(0)


add wave -noupdate -divider -height 20 "Control"
add wave -position end -label "tx_reset" sim:/mgt_testbench/tx_reset
add wave -position end -label "rx_reset" sim:/mgt_testbench/rx_reset
add wave -position end -label "Bit Slip" sim:/mgt_testbench/bitSlip


add wave -noupdate -divider -height 20 "TX"
add wave -position end -label "TX ready" sim:/mgt_testbench/tx_ready
add wave -position end -label "TX parallel data (input)" sim:/mgt_testbench/tx_parallel_data
add wave -position end -label "TX serial data (output)" sim:/mgt_testbench/tx_serial_data


add wave -noupdate -divider -height 20 "RX"
add wave -position end -label "RX ready" sim:/mgt_testbench/rx_ready
add wave -position end -label "RX parallel data (output)" sim:/mgt_testbench/rx_parallel_data
add wave -position end -label "RX serial data (input)" sim:/mgt_testbench/rx_serial_data


echo "testbench for MGT prototype

These functions are available:


tx\_isReady
	returns 1 of tx is ready

rx\_isReady
	returns 1 of rx is ready

tx\_waitForReady
	runs the simulation until tx is ready (approx 19us)

rx\_waitForReady
	runs the simulation until rx is ready (approx 34us)

tx\_reset
	sends reset signal to tx portion of transceiver

rx\_reset
	sends reset signal to rx portion of transceiver

bitSlip
	sends the bitslip signal to the rx portion of transceiver
"







