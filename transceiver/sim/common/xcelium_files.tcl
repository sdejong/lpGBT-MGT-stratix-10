source [file join [file dirname [info script]] ./../../../ip/transceiver/RX_RESET/sim/common/xcelium_files.tcl]
source [file join [file dirname [info script]] ./../../../ip/transceiver/PHY/sim/common/xcelium_files.tcl]
source [file join [file dirname [info script]] ./../../../ip/transceiver/CLK/sim/common/xcelium_files.tcl]
source [file join [file dirname [info script]] ./../../../ip/transceiver/TX_RESET/sim/common/xcelium_files.tcl]
source [file join [file dirname [info script]] ./../../../ip/transceiver/PLL/sim/common/xcelium_files.tcl]

namespace eval transceiver {
  proc get_design_libraries {} {
    set libraries [dict create]
    set libraries [dict merge $libraries [RX_RESET::get_design_libraries]]
    set libraries [dict merge $libraries [PHY::get_design_libraries]]
    set libraries [dict merge $libraries [CLK::get_design_libraries]]
    set libraries [dict merge $libraries [TX_RESET::get_design_libraries]]
    set libraries [dict merge $libraries [PLL::get_design_libraries]]
    dict set libraries transceiver 1
    return $libraries
  }
  
  proc get_memory_files {QSYS_SIMDIR} {
    set memory_files [list]
    set memory_files [concat $memory_files [RX_RESET::get_memory_files "$QSYS_SIMDIR/../../ip/transceiver/RX_RESET/sim/"]]
    set memory_files [concat $memory_files [PHY::get_memory_files "$QSYS_SIMDIR/../../ip/transceiver/PHY/sim/"]]
    set memory_files [concat $memory_files [CLK::get_memory_files "$QSYS_SIMDIR/../../ip/transceiver/CLK/sim/"]]
    set memory_files [concat $memory_files [TX_RESET::get_memory_files "$QSYS_SIMDIR/../../ip/transceiver/TX_RESET/sim/"]]
    set memory_files [concat $memory_files [PLL::get_memory_files "$QSYS_SIMDIR/../../ip/transceiver/PLL/sim/"]]
    return $memory_files
  }
  
  proc get_common_design_files {USER_DEFINED_COMPILE_OPTIONS USER_DEFINED_VERILOG_COMPILE_OPTIONS USER_DEFINED_VHDL_COMPILE_OPTIONS QSYS_SIMDIR} {
    set design_files [dict create]
    set design_files [dict merge $design_files [RX_RESET::get_common_design_files $USER_DEFINED_COMPILE_OPTIONS $USER_DEFINED_VERILOG_COMPILE_OPTIONS $USER_DEFINED_VHDL_COMPILE_OPTIONS "$QSYS_SIMDIR/../../ip/transceiver/RX_RESET/sim/"]]
    set design_files [dict merge $design_files [PHY::get_common_design_files $USER_DEFINED_COMPILE_OPTIONS $USER_DEFINED_VERILOG_COMPILE_OPTIONS $USER_DEFINED_VHDL_COMPILE_OPTIONS "$QSYS_SIMDIR/../../ip/transceiver/PHY/sim/"]]
    set design_files [dict merge $design_files [CLK::get_common_design_files $USER_DEFINED_COMPILE_OPTIONS $USER_DEFINED_VERILOG_COMPILE_OPTIONS $USER_DEFINED_VHDL_COMPILE_OPTIONS "$QSYS_SIMDIR/../../ip/transceiver/CLK/sim/"]]
    set design_files [dict merge $design_files [TX_RESET::get_common_design_files $USER_DEFINED_COMPILE_OPTIONS $USER_DEFINED_VERILOG_COMPILE_OPTIONS $USER_DEFINED_VHDL_COMPILE_OPTIONS "$QSYS_SIMDIR/../../ip/transceiver/TX_RESET/sim/"]]
    set design_files [dict merge $design_files [PLL::get_common_design_files $USER_DEFINED_COMPILE_OPTIONS $USER_DEFINED_VERILOG_COMPILE_OPTIONS $USER_DEFINED_VHDL_COMPILE_OPTIONS "$QSYS_SIMDIR/../../ip/transceiver/PLL/sim/"]]
    return $design_files
  }
  
  proc get_design_files {USER_DEFINED_COMPILE_OPTIONS USER_DEFINED_VERILOG_COMPILE_OPTIONS USER_DEFINED_VHDL_COMPILE_OPTIONS QSYS_SIMDIR} {
    set design_files [list]
    set design_files [concat $design_files [RX_RESET::get_design_files $USER_DEFINED_COMPILE_OPTIONS $USER_DEFINED_VERILOG_COMPILE_OPTIONS $USER_DEFINED_VHDL_COMPILE_OPTIONS "$QSYS_SIMDIR/../../ip/transceiver/RX_RESET/sim/"]]
    set design_files [concat $design_files [PHY::get_design_files $USER_DEFINED_COMPILE_OPTIONS $USER_DEFINED_VERILOG_COMPILE_OPTIONS $USER_DEFINED_VHDL_COMPILE_OPTIONS "$QSYS_SIMDIR/../../ip/transceiver/PHY/sim/"]]
    set design_files [concat $design_files [CLK::get_design_files $USER_DEFINED_COMPILE_OPTIONS $USER_DEFINED_VERILOG_COMPILE_OPTIONS $USER_DEFINED_VHDL_COMPILE_OPTIONS "$QSYS_SIMDIR/../../ip/transceiver/CLK/sim/"]]
    set design_files [concat $design_files [TX_RESET::get_design_files $USER_DEFINED_COMPILE_OPTIONS $USER_DEFINED_VERILOG_COMPILE_OPTIONS $USER_DEFINED_VHDL_COMPILE_OPTIONS "$QSYS_SIMDIR/../../ip/transceiver/TX_RESET/sim/"]]
    set design_files [concat $design_files [PLL::get_design_files $USER_DEFINED_COMPILE_OPTIONS $USER_DEFINED_VERILOG_COMPILE_OPTIONS $USER_DEFINED_VHDL_COMPILE_OPTIONS "$QSYS_SIMDIR/../../ip/transceiver/PLL/sim/"]]
    lappend design_files "xmvhdl -v93 $USER_DEFINED_VHDL_COMPILE_OPTIONS $USER_DEFINED_COMPILE_OPTIONS  \"$QSYS_SIMDIR/transceiver.vhd\"  -work transceiver"
    return $design_files
  }
  
  proc get_elab_options {SIMULATOR_TOOL_BITNESS} {
    set ELAB_OPTIONS ""
    append ELAB_OPTIONS [RX_RESET::get_elab_options $SIMULATOR_TOOL_BITNESS]
    append ELAB_OPTIONS [PHY::get_elab_options $SIMULATOR_TOOL_BITNESS]
    append ELAB_OPTIONS [CLK::get_elab_options $SIMULATOR_TOOL_BITNESS]
    append ELAB_OPTIONS [TX_RESET::get_elab_options $SIMULATOR_TOOL_BITNESS]
    append ELAB_OPTIONS [PLL::get_elab_options $SIMULATOR_TOOL_BITNESS]
    if ![ string match "bit_64" $SIMULATOR_TOOL_BITNESS ] {
    } else {
    }
    return $ELAB_OPTIONS
  }
  
  
  proc get_sim_options {SIMULATOR_TOOL_BITNESS} {
    set SIM_OPTIONS ""
    append SIM_OPTIONS [RX_RESET::get_sim_options $SIMULATOR_TOOL_BITNESS]
    append SIM_OPTIONS [PHY::get_sim_options $SIMULATOR_TOOL_BITNESS]
    append SIM_OPTIONS [CLK::get_sim_options $SIMULATOR_TOOL_BITNESS]
    append SIM_OPTIONS [TX_RESET::get_sim_options $SIMULATOR_TOOL_BITNESS]
    append SIM_OPTIONS [PLL::get_sim_options $SIMULATOR_TOOL_BITNESS]
    if ![ string match "bit_64" $SIMULATOR_TOOL_BITNESS ] {
    } else {
    }
    return $SIM_OPTIONS
  }
  
  
  proc get_env_variables {SIMULATOR_TOOL_BITNESS} {
    set ENV_VARIABLES [dict create]
    set LD_LIBRARY_PATH [dict create]
    set LD_LIBRARY_PATH [dict merge $LD_LIBRARY_PATH [dict get [RX_RESET::get_env_variables $SIMULATOR_TOOL_BITNESS] "LD_LIBRARY_PATH"]]
    set LD_LIBRARY_PATH [dict merge $LD_LIBRARY_PATH [dict get [PHY::get_env_variables $SIMULATOR_TOOL_BITNESS] "LD_LIBRARY_PATH"]]
    set LD_LIBRARY_PATH [dict merge $LD_LIBRARY_PATH [dict get [CLK::get_env_variables $SIMULATOR_TOOL_BITNESS] "LD_LIBRARY_PATH"]]
    set LD_LIBRARY_PATH [dict merge $LD_LIBRARY_PATH [dict get [TX_RESET::get_env_variables $SIMULATOR_TOOL_BITNESS] "LD_LIBRARY_PATH"]]
    set LD_LIBRARY_PATH [dict merge $LD_LIBRARY_PATH [dict get [PLL::get_env_variables $SIMULATOR_TOOL_BITNESS] "LD_LIBRARY_PATH"]]
    dict set ENV_VARIABLES "LD_LIBRARY_PATH" $LD_LIBRARY_PATH
    if ![ string match "bit_64" $SIMULATOR_TOOL_BITNESS ] {
    } else {
    }
    return $ENV_VARIABLES
  }
  
  
}
