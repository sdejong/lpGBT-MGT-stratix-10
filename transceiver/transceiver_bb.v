module transceiver (
		input  wire        clk_clk,                                         //                     clk.clk
		input  wire [0:0]  pll_select_pll_select,                           //              pll_select.pll_select
		input  wire [0:0]  rx_bitslip_rx_bitslip,                           //              rx_bitslip.rx_bitslip
		output wire [0:0]  rx_clkout_clk,                                   //               rx_clkout.clk
		output wire [0:0]  rx_is_lockedtoref_rx_is_lockedtoref,             //       rx_is_lockedtoref.rx_is_lockedtoref
		output wire [31:0] rx_parallel_data_rx_parallel_data,               //        rx_parallel_data.rx_parallel_data
		output wire [0:0]  rx_ready_rx_ready,                               //                rx_ready.rx_ready
		input  wire        rx_reset_reset,                                  //                rx_reset.reset
		input  wire [0:0]  rx_serial_data_rx_serial_data,                   //          rx_serial_data.rx_serial_data
		output wire [0:0]  tx_clkout_clk,                                   //               tx_clkout.clk
		input  wire [31:0] tx_parallel_data_tx_parallel_data,               //        tx_parallel_data.tx_parallel_data
		output wire [0:0]  tx_ready_tx_ready,                               //                tx_ready.tx_ready
		input  wire        tx_reset_reset,                                  //                tx_reset.reset
		output wire [0:0]  tx_serial_data_tx_serial_data,                   //          tx_serial_data.tx_serial_data
		output wire [47:0] unused_rx_parallel_data_unused_rx_parallel_data, // unused_rx_parallel_data.unused_rx_parallel_data
		input  wire [47:0] unused_tx_parallel_data_unused_tx_parallel_data  // unused_tx_parallel_data.unused_tx_parallel_data
	);
endmodule

