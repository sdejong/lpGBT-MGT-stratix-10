	transceiver u0 (
		.clk_clk                                         (_connected_to_clk_clk_),                                         //   input,   width = 1,                     clk.clk
		.pll_select_pll_select                           (_connected_to_pll_select_pll_select_),                           //   input,   width = 1,              pll_select.pll_select
		.rx_bitslip_rx_bitslip                           (_connected_to_rx_bitslip_rx_bitslip_),                           //   input,   width = 1,              rx_bitslip.rx_bitslip
		.rx_clkout_clk                                   (_connected_to_rx_clkout_clk_),                                   //  output,   width = 1,               rx_clkout.clk
		.rx_is_lockedtoref_rx_is_lockedtoref             (_connected_to_rx_is_lockedtoref_rx_is_lockedtoref_),             //  output,   width = 1,       rx_is_lockedtoref.rx_is_lockedtoref
		.rx_parallel_data_rx_parallel_data               (_connected_to_rx_parallel_data_rx_parallel_data_),               //  output,  width = 32,        rx_parallel_data.rx_parallel_data
		.rx_ready_rx_ready                               (_connected_to_rx_ready_rx_ready_),                               //  output,   width = 1,                rx_ready.rx_ready
		.rx_reset_reset                                  (_connected_to_rx_reset_reset_),                                  //   input,   width = 1,                rx_reset.reset
		.rx_serial_data_rx_serial_data                   (_connected_to_rx_serial_data_rx_serial_data_),                   //   input,   width = 1,          rx_serial_data.rx_serial_data
		.tx_clkout_clk                                   (_connected_to_tx_clkout_clk_),                                   //  output,   width = 1,               tx_clkout.clk
		.tx_parallel_data_tx_parallel_data               (_connected_to_tx_parallel_data_tx_parallel_data_),               //   input,  width = 32,        tx_parallel_data.tx_parallel_data
		.tx_ready_tx_ready                               (_connected_to_tx_ready_tx_ready_),                               //  output,   width = 1,                tx_ready.tx_ready
		.tx_reset_reset                                  (_connected_to_tx_reset_reset_),                                  //   input,   width = 1,                tx_reset.reset
		.tx_serial_data_tx_serial_data                   (_connected_to_tx_serial_data_tx_serial_data_),                   //  output,   width = 1,          tx_serial_data.tx_serial_data
		.unused_rx_parallel_data_unused_rx_parallel_data (_connected_to_unused_rx_parallel_data_unused_rx_parallel_data_), //  output,  width = 48, unused_rx_parallel_data.unused_rx_parallel_data
		.unused_tx_parallel_data_unused_tx_parallel_data (_connected_to_unused_tx_parallel_data_unused_tx_parallel_data_)  //   input,  width = 48, unused_tx_parallel_data.unused_tx_parallel_data
	);

