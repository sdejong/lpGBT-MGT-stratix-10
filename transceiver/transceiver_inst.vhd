	component transceiver is
		port (
			clk_clk                                         : in  std_logic                     := 'X';             -- clk
			pll_select_pll_select                           : in  std_logic_vector(0 downto 0)  := (others => 'X'); -- pll_select
			rx_bitslip_rx_bitslip                           : in  std_logic_vector(0 downto 0)  := (others => 'X'); -- rx_bitslip
			rx_clkout_clk                                   : out std_logic_vector(0 downto 0);                     -- clk
			rx_is_lockedtoref_rx_is_lockedtoref             : out std_logic_vector(0 downto 0);                     -- rx_is_lockedtoref
			rx_parallel_data_rx_parallel_data               : out std_logic_vector(31 downto 0);                    -- rx_parallel_data
			rx_ready_rx_ready                               : out std_logic_vector(0 downto 0);                     -- rx_ready
			rx_reset_reset                                  : in  std_logic                     := 'X';             -- reset
			rx_serial_data_rx_serial_data                   : in  std_logic_vector(0 downto 0)  := (others => 'X'); -- rx_serial_data
			tx_clkout_clk                                   : out std_logic_vector(0 downto 0);                     -- clk
			tx_parallel_data_tx_parallel_data               : in  std_logic_vector(31 downto 0) := (others => 'X'); -- tx_parallel_data
			tx_ready_tx_ready                               : out std_logic_vector(0 downto 0);                     -- tx_ready
			tx_reset_reset                                  : in  std_logic                     := 'X';             -- reset
			tx_serial_data_tx_serial_data                   : out std_logic_vector(0 downto 0);                     -- tx_serial_data
			unused_rx_parallel_data_unused_rx_parallel_data : out std_logic_vector(47 downto 0);                    -- unused_rx_parallel_data
			unused_tx_parallel_data_unused_tx_parallel_data : in  std_logic_vector(47 downto 0) := (others => 'X')  -- unused_tx_parallel_data
		);
	end component transceiver;

	u0 : component transceiver
		port map (
			clk_clk                                         => CONNECTED_TO_clk_clk,                                         --                     clk.clk
			pll_select_pll_select                           => CONNECTED_TO_pll_select_pll_select,                           --              pll_select.pll_select
			rx_bitslip_rx_bitslip                           => CONNECTED_TO_rx_bitslip_rx_bitslip,                           --              rx_bitslip.rx_bitslip
			rx_clkout_clk                                   => CONNECTED_TO_rx_clkout_clk,                                   --               rx_clkout.clk
			rx_is_lockedtoref_rx_is_lockedtoref             => CONNECTED_TO_rx_is_lockedtoref_rx_is_lockedtoref,             --       rx_is_lockedtoref.rx_is_lockedtoref
			rx_parallel_data_rx_parallel_data               => CONNECTED_TO_rx_parallel_data_rx_parallel_data,               --        rx_parallel_data.rx_parallel_data
			rx_ready_rx_ready                               => CONNECTED_TO_rx_ready_rx_ready,                               --                rx_ready.rx_ready
			rx_reset_reset                                  => CONNECTED_TO_rx_reset_reset,                                  --                rx_reset.reset
			rx_serial_data_rx_serial_data                   => CONNECTED_TO_rx_serial_data_rx_serial_data,                   --          rx_serial_data.rx_serial_data
			tx_clkout_clk                                   => CONNECTED_TO_tx_clkout_clk,                                   --               tx_clkout.clk
			tx_parallel_data_tx_parallel_data               => CONNECTED_TO_tx_parallel_data_tx_parallel_data,               --        tx_parallel_data.tx_parallel_data
			tx_ready_tx_ready                               => CONNECTED_TO_tx_ready_tx_ready,                               --                tx_ready.tx_ready
			tx_reset_reset                                  => CONNECTED_TO_tx_reset_reset,                                  --                tx_reset.reset
			tx_serial_data_tx_serial_data                   => CONNECTED_TO_tx_serial_data_tx_serial_data,                   --          tx_serial_data.tx_serial_data
			unused_rx_parallel_data_unused_rx_parallel_data => CONNECTED_TO_unused_rx_parallel_data_unused_rx_parallel_data, -- unused_rx_parallel_data.unused_rx_parallel_data
			unused_tx_parallel_data_unused_tx_parallel_data => CONNECTED_TO_unused_tx_parallel_data_unused_tx_parallel_data  -- unused_tx_parallel_data.unused_tx_parallel_data
		);

