
proc get_memory_files {QSYS_SIMDIR} {
  set memory_files [list]
  return $memory_files
}

proc get_common_design_files {QSYS_SIMDIR} {
  set design_files [dict create]
  dict set design_files "altera_common_sv_packages::altera_xcvr_native_s10_functions_h" "$QSYS_SIMDIR/ip/transceiver/PHY/altera_xcvr_native_s10_htile_180/sim/altera_xcvr_native_s10_functions_h.sv"
  return $design_files
}

proc get_design_files {QSYS_SIMDIR} {
  set design_files [dict create]
  error "Skipping VCS script generation since VHDL file $QSYS_SIMDIR/ip/transceiver/PHY/sim/PHY.vhd is required for simulation"
}

proc get_elab_options {SIMULATOR_TOOL_BITNESS} {
  set ELAB_OPTIONS ""
  if ![ string match "bit_64" $SIMULATOR_TOOL_BITNESS ] {
  } else {
  }
  return $ELAB_OPTIONS
}


proc get_sim_options {SIMULATOR_TOOL_BITNESS} {
  set SIM_OPTIONS ""
  if ![ string match "bit_64" $SIMULATOR_TOOL_BITNESS ] {
  } else {
  }
  return $SIM_OPTIONS
}


proc get_env_variables {SIMULATOR_TOOL_BITNESS} {
  set ENV_VARIABLES [dict create]
  set LD_LIBRARY_PATH [dict create]
  dict set ENV_VARIABLES "LD_LIBRARY_PATH" $LD_LIBRARY_PATH
  if ![ string match "bit_64" $SIMULATOR_TOOL_BITNESS ] {
  } else {
  }
  return $ENV_VARIABLES
}


