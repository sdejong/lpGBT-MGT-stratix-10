
proc get_design_libraries {} {
  set libraries [dict create]
  dict set libraries altera_common_sv_packages         1
  dict set libraries altera_xcvr_native_s10_htile_180  1
  dict set libraries PHY                               1
  dict set libraries altera_xcvr_cdr_pll_s10_htile_180 1
  dict set libraries PLL                               1
  dict set libraries CLK                               1
  dict set libraries altera_xcvr_reset_control_s10_180 1
  dict set libraries RX_RESET                          1
  dict set libraries TX_RESET                          1
  dict set libraries transceiver                       1
  return $libraries
}

proc get_memory_files {QSYS_SIMDIR} {
  set memory_files [list]
  return $memory_files
}

proc get_common_design_files {USER_DEFINED_COMPILE_OPTIONS USER_DEFINED_VERILOG_COMPILE_OPTIONS USER_DEFINED_VHDL_COMPILE_OPTIONS QSYS_SIMDIR} {
  set design_files [dict create]
  dict set design_files "altera_common_sv_packages::altera_xcvr_native_s10_functions_h" "vlog  $USER_DEFINED_VERILOG_COMPILE_OPTIONS $USER_DEFINED_COMPILE_OPTIONS  \"[normalize_path "$QSYS_SIMDIR/ip/transceiver/PHY/altera_xcvr_native_s10_htile_180/sim/altera_xcvr_native_s10_functions_h.sv"]\"  -work altera_common_sv_packages"
  return $design_files
}

proc get_design_files {USER_DEFINED_COMPILE_OPTIONS USER_DEFINED_VERILOG_COMPILE_OPTIONS USER_DEFINED_VHDL_COMPILE_OPTIONS QSYS_SIMDIR} {
  set design_files [list]
  lappend design_files "vlog  $USER_DEFINED_VERILOG_COMPILE_OPTIONS $USER_DEFINED_COMPILE_OPTIONS  \"[normalize_path "$QSYS_SIMDIR/ip/transceiver/PHY/altera_xcvr_native_s10_htile_180/sim/alt_xcvr_arbiter.sv"]\" -l altera_common_sv_packages -work altera_xcvr_native_s10_htile_180"                               
  lappend design_files "vlog -v2k5 $USER_DEFINED_VERILOG_COMPILE_OPTIONS $USER_DEFINED_COMPILE_OPTIONS  \"[normalize_path "$QSYS_SIMDIR/ip/transceiver/PHY/altera_xcvr_native_s10_htile_180/sim/altera_std_synchronizer_nocut.v"]\"  -work altera_xcvr_native_s10_htile_180"                                          
  lappend design_files "vlog  $USER_DEFINED_VERILOG_COMPILE_OPTIONS $USER_DEFINED_COMPILE_OPTIONS  \"[normalize_path "$QSYS_SIMDIR/ip/transceiver/PHY/altera_xcvr_native_s10_htile_180/sim/alt_xcvr_resync_std.sv"]\" -l altera_common_sv_packages -work altera_xcvr_native_s10_htile_180"                            
  lappend design_files "vlog  $USER_DEFINED_VERILOG_COMPILE_OPTIONS $USER_DEFINED_COMPILE_OPTIONS  \"[normalize_path "$QSYS_SIMDIR/ip/transceiver/PHY/altera_xcvr_native_s10_htile_180/sim/alt_xcvr_reset_counter_s10.sv"]\" -l altera_common_sv_packages -work altera_xcvr_native_s10_htile_180"                     
  lappend design_files "vlog  $USER_DEFINED_VERILOG_COMPILE_OPTIONS $USER_DEFINED_COMPILE_OPTIONS  \"[normalize_path "$QSYS_SIMDIR/ip/transceiver/PHY/altera_xcvr_native_s10_htile_180/sim/s10_avmm_h.sv"]\" -l altera_common_sv_packages -work altera_xcvr_native_s10_htile_180"                                     
  lappend design_files "vlog  $USER_DEFINED_VERILOG_COMPILE_OPTIONS $USER_DEFINED_COMPILE_OPTIONS  \"[normalize_path "$QSYS_SIMDIR/ip/transceiver/PHY/altera_xcvr_native_s10_htile_180/sim/alt_xcvr_native_avmm_csr.sv"]\" -l altera_common_sv_packages -work altera_xcvr_native_s10_htile_180"                       
  lappend design_files "vlog  $USER_DEFINED_VERILOG_COMPILE_OPTIONS $USER_DEFINED_COMPILE_OPTIONS  \"[normalize_path "$QSYS_SIMDIR/ip/transceiver/PHY/altera_xcvr_native_s10_htile_180/sim/alt_xcvr_native_prbs_accum.sv"]\" -l altera_common_sv_packages -work altera_xcvr_native_s10_htile_180"                     
  lappend design_files "vlog  $USER_DEFINED_VERILOG_COMPILE_OPTIONS $USER_DEFINED_COMPILE_OPTIONS  \"[normalize_path "$QSYS_SIMDIR/ip/transceiver/PHY/altera_xcvr_native_s10_htile_180/sim/alt_xcvr_native_odi_accel.sv"]\" -l altera_common_sv_packages -work altera_xcvr_native_s10_htile_180"                      
  lappend design_files "vlog  $USER_DEFINED_VERILOG_COMPILE_OPTIONS $USER_DEFINED_COMPILE_OPTIONS  \"[normalize_path "$QSYS_SIMDIR/ip/transceiver/PHY/altera_xcvr_native_s10_htile_180/sim/alt_xcvr_native_rcfg_arb.sv"]\" -l altera_common_sv_packages -work altera_xcvr_native_s10_htile_180"                       
  lappend design_files "vlog  $USER_DEFINED_VERILOG_COMPILE_OPTIONS $USER_DEFINED_COMPILE_OPTIONS  \"[normalize_path "$QSYS_SIMDIR/ip/transceiver/PHY/altera_xcvr_native_s10_htile_180/sim/alt_xcvr_native_anlg_reset_seq.sv"]\" -l altera_common_sv_packages -work altera_xcvr_native_s10_htile_180"                 
  lappend design_files "vlog  $USER_DEFINED_VERILOG_COMPILE_OPTIONS $USER_DEFINED_COMPILE_OPTIONS  \"[normalize_path "$QSYS_SIMDIR/ip/transceiver/PHY/altera_xcvr_native_s10_htile_180/sim/alt_xcvr_native_dig_reset_seq.sv"]\" -l altera_common_sv_packages -work altera_xcvr_native_s10_htile_180"                  
  lappend design_files "vlog  $USER_DEFINED_VERILOG_COMPILE_OPTIONS $USER_DEFINED_COMPILE_OPTIONS  \"[normalize_path "$QSYS_SIMDIR/ip/transceiver/PHY/altera_xcvr_native_s10_htile_180/sim/alt_xcvr_native_reset_seq.sv"]\" -l altera_common_sv_packages -work altera_xcvr_native_s10_htile_180"                      
  lappend design_files "vlog  $USER_DEFINED_VERILOG_COMPILE_OPTIONS $USER_DEFINED_COMPILE_OPTIONS  \"[normalize_path "$QSYS_SIMDIR/ip/transceiver/PHY/altera_xcvr_native_s10_htile_180/sim/alt_xcvr_native_anlg_reset_seq_wrapper.sv"]\" -l altera_common_sv_packages -work altera_xcvr_native_s10_htile_180"         
  lappend design_files "vlog  $USER_DEFINED_VERILOG_COMPILE_OPTIONS $USER_DEFINED_COMPILE_OPTIONS  \"[normalize_path "$QSYS_SIMDIR/ip/transceiver/PHY/altera_xcvr_native_s10_htile_180/sim/PHY_altera_xcvr_native_s10_htile_180_dtnc26i.sv"]\" -l altera_common_sv_packages -work altera_xcvr_native_s10_htile_180"   
  lappend design_files "vlog  $USER_DEFINED_VERILOG_COMPILE_OPTIONS $USER_DEFINED_COMPILE_OPTIONS  \"[normalize_path "$QSYS_SIMDIR/ip/transceiver/PHY/altera_xcvr_native_s10_htile_180/sim/alt_xcvr_native_rcfg_opt_logic_dtnc26i.sv"]\" -l altera_common_sv_packages -work altera_xcvr_native_s10_htile_180"         
  lappend design_files "vcom $USER_DEFINED_VHDL_COMPILE_OPTIONS $USER_DEFINED_COMPILE_OPTIONS  \"[normalize_path "$QSYS_SIMDIR/ip/transceiver/PHY/sim/PHY.vhd"]\"  -work PHY"                                                                                                                                         
  lappend design_files "vlog  $USER_DEFINED_VERILOG_COMPILE_OPTIONS $USER_DEFINED_COMPILE_OPTIONS  \"[normalize_path "$QSYS_SIMDIR/ip/transceiver/PLL/altera_xcvr_cdr_pll_s10_htile_180/sim/alt_xcvr_resync.sv"]\" -l altera_common_sv_packages -work altera_xcvr_cdr_pll_s10_htile_180"                              
  lappend design_files "vlog  $USER_DEFINED_VERILOG_COMPILE_OPTIONS $USER_DEFINED_COMPILE_OPTIONS  \"[normalize_path "$QSYS_SIMDIR/ip/transceiver/PLL/altera_xcvr_cdr_pll_s10_htile_180/sim/alt_xcvr_arbiter.sv"]\" -l altera_common_sv_packages -work altera_xcvr_cdr_pll_s10_htile_180"                             
  lappend design_files "vlog  $USER_DEFINED_VERILOG_COMPILE_OPTIONS $USER_DEFINED_COMPILE_OPTIONS  \"[normalize_path "$QSYS_SIMDIR/ip/transceiver/PLL/altera_xcvr_cdr_pll_s10_htile_180/sim/s10_avmm_h.sv"]\" -l altera_common_sv_packages -work altera_xcvr_cdr_pll_s10_htile_180"                                   
  lappend design_files "vlog  $USER_DEFINED_VERILOG_COMPILE_OPTIONS $USER_DEFINED_COMPILE_OPTIONS  \"[normalize_path "$QSYS_SIMDIR/ip/transceiver/PLL/altera_xcvr_cdr_pll_s10_htile_180/sim/alt_xcvr_pll_rcfg_arb.sv"]\" -l altera_common_sv_packages -work altera_xcvr_cdr_pll_s10_htile_180"                        
  lappend design_files "vlog  $USER_DEFINED_VERILOG_COMPILE_OPTIONS $USER_DEFINED_COMPILE_OPTIONS  \"[normalize_path "$QSYS_SIMDIR/ip/transceiver/PLL/altera_xcvr_cdr_pll_s10_htile_180/sim/alt_xcvr_pll_embedded_debug.sv"]\" -l altera_common_sv_packages -work altera_xcvr_cdr_pll_s10_htile_180"                  
  lappend design_files "vlog  $USER_DEFINED_VERILOG_COMPILE_OPTIONS $USER_DEFINED_COMPILE_OPTIONS  \"[normalize_path "$QSYS_SIMDIR/ip/transceiver/PLL/altera_xcvr_cdr_pll_s10_htile_180/sim/alt_xcvr_pll_avmm_csr.sv"]\" -l altera_common_sv_packages -work altera_xcvr_cdr_pll_s10_htile_180"                        
  lappend design_files "vlog  $USER_DEFINED_VERILOG_COMPILE_OPTIONS $USER_DEFINED_COMPILE_OPTIONS  \"[normalize_path "$QSYS_SIMDIR/ip/transceiver/PLL/altera_xcvr_cdr_pll_s10_htile_180/sim/PLL_altera_xcvr_cdr_pll_s10_htile_180_vzmxkfa.sv"]\" -l altera_common_sv_packages -work altera_xcvr_cdr_pll_s10_htile_180"
  lappend design_files "vlog  $USER_DEFINED_VERILOG_COMPILE_OPTIONS $USER_DEFINED_COMPILE_OPTIONS  \"[normalize_path "$QSYS_SIMDIR/ip/transceiver/PLL/altera_xcvr_cdr_pll_s10_htile_180/sim/alt_xcvr_pll_rcfg_opt_logic_vzmxkfa.sv"]\" -l altera_common_sv_packages -work altera_xcvr_cdr_pll_s10_htile_180"          
  lappend design_files "vcom $USER_DEFINED_VHDL_COMPILE_OPTIONS $USER_DEFINED_COMPILE_OPTIONS  \"[normalize_path "$QSYS_SIMDIR/ip/transceiver/PLL/sim/PLL.vhd"]\"  -work PLL"                                                                                                                                         
  lappend design_files "vcom $USER_DEFINED_VHDL_COMPILE_OPTIONS $USER_DEFINED_COMPILE_OPTIONS  \"[normalize_path "$QSYS_SIMDIR/ip/transceiver/CLK/sim/CLK.vhd"]\"  -work CLK"                                                                                                                                         
  lappend design_files "vlog -v2k5 $USER_DEFINED_VERILOG_COMPILE_OPTIONS $USER_DEFINED_COMPILE_OPTIONS  \"[normalize_path "$QSYS_SIMDIR/ip/transceiver/RX_RESET/altera_xcvr_reset_control_s10_180/sim/altera_std_synchronizer_nocut.v"]\"  -work altera_xcvr_reset_control_s10_180"                                   
  lappend design_files "vlog  $USER_DEFINED_VERILOG_COMPILE_OPTIONS $USER_DEFINED_COMPILE_OPTIONS  \"[normalize_path "$QSYS_SIMDIR/ip/transceiver/RX_RESET/altera_xcvr_reset_control_s10_180/sim/alt_xcvr_resync_std.sv"]\"  -work altera_xcvr_reset_control_s10_180"                                                 
  lappend design_files "vlog  $USER_DEFINED_VERILOG_COMPILE_OPTIONS $USER_DEFINED_COMPILE_OPTIONS  \"[normalize_path "$QSYS_SIMDIR/ip/transceiver/RX_RESET/altera_xcvr_reset_control_s10_180/sim/altera_xcvr_reset_control_s10.sv"]\"  -work altera_xcvr_reset_control_s10_180"                                       
  lappend design_files "vlog  $USER_DEFINED_VERILOG_COMPILE_OPTIONS $USER_DEFINED_COMPILE_OPTIONS  \"[normalize_path "$QSYS_SIMDIR/ip/transceiver/RX_RESET/altera_xcvr_reset_control_s10_180/sim/alt_xcvr_reset_counter_s10.sv"]\"  -work altera_xcvr_reset_control_s10_180"                                          
  lappend design_files "vcom $USER_DEFINED_VHDL_COMPILE_OPTIONS $USER_DEFINED_COMPILE_OPTIONS  \"[normalize_path "$QSYS_SIMDIR/ip/transceiver/RX_RESET/sim/RX_RESET.vhd"]\"  -work RX_RESET"                                                                                                                          
  lappend design_files "vlog -v2k5 $USER_DEFINED_VERILOG_COMPILE_OPTIONS $USER_DEFINED_COMPILE_OPTIONS  \"[normalize_path "$QSYS_SIMDIR/ip/transceiver/TX_RESET/altera_xcvr_reset_control_s10_180/sim/altera_std_synchronizer_nocut.v"]\"  -work altera_xcvr_reset_control_s10_180"                                   
  lappend design_files "vlog  $USER_DEFINED_VERILOG_COMPILE_OPTIONS $USER_DEFINED_COMPILE_OPTIONS  \"[normalize_path "$QSYS_SIMDIR/ip/transceiver/TX_RESET/altera_xcvr_reset_control_s10_180/sim/alt_xcvr_resync_std.sv"]\"  -work altera_xcvr_reset_control_s10_180"                                                 
  lappend design_files "vlog  $USER_DEFINED_VERILOG_COMPILE_OPTIONS $USER_DEFINED_COMPILE_OPTIONS  \"[normalize_path "$QSYS_SIMDIR/ip/transceiver/TX_RESET/altera_xcvr_reset_control_s10_180/sim/altera_xcvr_reset_control_s10.sv"]\"  -work altera_xcvr_reset_control_s10_180"                                       
  lappend design_files "vlog  $USER_DEFINED_VERILOG_COMPILE_OPTIONS $USER_DEFINED_COMPILE_OPTIONS  \"[normalize_path "$QSYS_SIMDIR/ip/transceiver/TX_RESET/altera_xcvr_reset_control_s10_180/sim/alt_xcvr_reset_counter_s10.sv"]\"  -work altera_xcvr_reset_control_s10_180"                                          
  lappend design_files "vcom $USER_DEFINED_VHDL_COMPILE_OPTIONS $USER_DEFINED_COMPILE_OPTIONS  \"[normalize_path "$QSYS_SIMDIR/ip/transceiver/TX_RESET/sim/TX_RESET.vhd"]\"  -work TX_RESET"                                                                                                                          
  lappend design_files "vcom $USER_DEFINED_VHDL_COMPILE_OPTIONS $USER_DEFINED_COMPILE_OPTIONS  \"[normalize_path "$QSYS_SIMDIR/transceiver/sim/transceiver.vhd"]\"  -work transceiver"                                                                                                                                
  return $design_files
}

proc get_elab_options {SIMULATOR_TOOL_BITNESS} {
  set ELAB_OPTIONS ""
  if ![ string match "bit_64" $SIMULATOR_TOOL_BITNESS ] {
  } else {
  }
  return $ELAB_OPTIONS
}


proc get_sim_options {SIMULATOR_TOOL_BITNESS} {
  set SIM_OPTIONS ""
  if ![ string match "bit_64" $SIMULATOR_TOOL_BITNESS ] {
  } else {
  }
  return $SIM_OPTIONS
}


proc get_env_variables {SIMULATOR_TOOL_BITNESS} {
  set ENV_VARIABLES [dict create]
  set LD_LIBRARY_PATH [dict create]
  dict set ENV_VARIABLES "LD_LIBRARY_PATH" $LD_LIBRARY_PATH
  if ![ string match "bit_64" $SIMULATOR_TOOL_BITNESS ] {
  } else {
  }
  return $ENV_VARIABLES
}


proc normalize_path {FILEPATH} {
    if {[catch { package require fileutil } err]} { 
        return $FILEPATH 
    } 
    set path [fileutil::lexnormalize [file join [pwd] $FILEPATH]]  
    if {[file pathtype $FILEPATH] eq "relative"} { 
        set path [fileutil::relative [pwd] $path] 
    } 
    return $path 
} 
