--`timescale 1ms / 1ps 

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity MGT is
	port(
	     -- Reset
        rst_txRst_i                    : in  std_logic;
        rst_rxRst_i                    : in  std_logic;
        
        -- Clocks
        clk_mgt_reference_i            : in  std_logic;  -- 320MHz
        
        -- PCS Data
        data_txword_i                  : in  std_logic_vector(31 downto 0);
        data_rxword_o                  : out std_logic_vector(31 downto 0);
        
        -- Serial lane
        ser_tx_o                       : out std_logic;
        ser_rx_i                       : in  std_logic;
        
        -- Status
        sta_mgtTxRdy_o                 : out std_logic;
        sta_mgtRxRdy_o                 : out std_logic;
        
        -- Control
        ctr_clkSlip_i                  : in  std_logic
	  
	);
end MGT;

architecture rtl of MGT is

	signal ser_tx_s  				: std_logic_vector (0 to 0);
	signal ser_rx_s  				: std_logic_vector (0 to 0);
	
	signal sta_mgtTxRdy_s   	: std_logic_vector (0 to 0);
	signal sta_mgtRxRdy_s   	: std_logic_vector (0 to 0);
	
	signal ctr_clkSlip_s  		: std_logic_vector (0 to 0);

	signal unused_TX_data_s 	: std_logic_vector(47 downto 0) := (others => '0');
	signal unused_RX_data_s 	: std_logic_vector(47 downto 0);
	
	signal tx_clk_s 				: std_logic_vector (0 to 0);
	signal rx_clk_s 				: std_logic_vector (0 to 0); 
	
	signal pll_select_s 			: std_logic_vector (0 to 0) := (others => '0');
	signal rx_is_lockedtoref_s : std_logic_vector (0 to 0);
	
   component transceiver is
        port (
            tx_reset_reset                                  : in  std_logic := 'X';
            rx_reset_reset                                  : in  std_logic := 'X';

            clk_clk                                         : in  std_logic := 'X';

            tx_parallel_data_tx_parallel_data               : in  std_logic_vector(31 downto 0) := (others => 'X');
            rx_parallel_data_rx_parallel_data               : out std_logic_vector(31 downto 0);

            tx_serial_data_tx_serial_data                   : out std_logic_vector(0 downto 0);
            rx_serial_data_rx_serial_data                   : in  std_logic_vector(0 downto 0) := (others => 'X');

            tx_ready_tx_ready                               : out std_logic_vector(0 downto 0);
            rx_ready_rx_ready                               : out std_logic_vector(0 downto 0);

            rx_bitslip_rx_bitslip                           : in  std_logic_vector(0 downto 0) := (others => 'X');
		
            pll_select_pll_select                           : in  std_logic_vector(0 downto 0) := (others => 'X');

            tx_clkout_clk                                   : out std_logic_vector(0 downto 0);
            rx_clkout_clk                                   : out std_logic_vector(0 downto 0);

            rx_is_lockedtoref_rx_is_lockedtoref             : out std_logic_vector(0 downto 0);

            unused_tx_parallel_data_unused_tx_parallel_data : in  std_logic_vector(47 downto 0) := (others => 'X');
            unused_rx_parallel_data_unused_rx_parallel_data : out std_logic_vector(47 downto 0)
				);
    end component transceiver;

		
begin

	ctr_clkSlip_s(0) <= ctr_clkSlip_i;
	
	sta_mgtTxRdy_o <= sta_mgtTxRdy_s(0);
	sta_mgtRxRdy_o <= sta_mgtRxRdy_s(0);

	ser_tx_o <= ser_tx_s(0);
	ser_rx_s(0) <= ser_rx_i;

    u0 : component transceiver
        port map (
            tx_reset_reset                                  => rst_txRst_i,
            rx_reset_reset                                  => rst_rxRst_i,        

            clk_clk                                         => clk_mgt_reference_i,

            tx_parallel_data_tx_parallel_data               => data_txword_i,
            rx_parallel_data_rx_parallel_data               => data_rxword_o, 
				
            tx_serial_data_tx_serial_data                   => ser_tx_s,
            rx_serial_data_rx_serial_data                   => ser_rx_s,
				
            tx_ready_tx_ready                               => sta_mgtTxRdy_s,
            rx_ready_rx_ready                               => sta_mgtRxRdy_s,

            rx_bitslip_rx_bitslip                           => ctr_clkSlip_s,


            pll_select_pll_select                           => pll_select_s,

            tx_clkout_clk                                   => tx_clk_s,
            rx_clkout_clk                                   => rx_clk_s,
            rx_is_lockedtoref_rx_is_lockedtoref             => rx_is_lockedtoref_s,

            unused_tx_parallel_data_unused_tx_parallel_data => unused_TX_data_s,
            unused_rx_parallel_data_unused_rx_parallel_data => unused_RX_data_s
        );

		  

end rtl;





