library ieee;
use ieee.math_real.all;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


entity MGT_testbench is
end MGT_testbench;


architecture behave of MGT_testbench is

	signal clk_mgt_320MHz_tx 					: std_logic :='1';
	signal clk_serial_rx 						: std_logic :='1';
	signal midWordTX 								: unsigned(31 downto 0) := x"F0F0F0F0";

	signal tx_reset                        : std_logic:='0';
	signal rx_reset                        : std_logic:='0';

	signal tx_parallel_data   					: std_logic_vector(31 downto 0);
	signal rx_parallel_data   					: std_logic_vector(31 downto 0);

	signal tx_serial_data       				: std_logic;
	signal rx_serial_data       				: std_logic := '0';

	signal rx_ready             		      : std_logic := '0';
	signal tx_ready                  		: std_logic := '0';

	signal bitSlip 								: std_logic := '0';
  
  COMPONENT MGT is
	port(
		     -- Reset
        rst_txRst_i                    : in  std_logic;
        rst_rxRst_i                    : in  std_logic;
        
        -- Clocks
		  clk_mgt_reference_i				: in  std_logic;  -- 320MHz
        
        -- PCS Data
        data_txword_i                  : in  std_logic_vector(31 downto 0);
        data_rxword_o                  : out std_logic_vector(31 downto 0);
        
        -- Serial lane
        ser_tx_o                       : out std_logic;
        ser_rx_i                       : in  std_logic;
        
        -- Status
        sta_mgtTxRdy_o                 : out std_logic;
        sta_mgtRxRdy_o                 : out std_logic;
        
        -- Control
        ctr_clkSlip_i                  : in  std_logic
	  
	);
	end COMPONENT;
	
	
begin

	clk_mgt_320MHz_tx <= not clk_mgt_320MHz_tx after 1.5625 ns;
	clk_serial_rx <= not clk_serial_rx after 0.048821 ns;


	rx_serial_data<=tx_serial_data;


	dataProc: process(clk_mgt_320MHz_tx)
	    begin
	      if rising_edge(clk_mgt_320MHz_tx) then
	        if midWordTX = x"FFFFFFFF" then
	          midWordTX    <= x"F0F0F0F0";
	        else
	          midWordTX    <= midWordTX + X"01010101";
	        end if;
	      end if;            
	    end process;

	    tx_parallel_data <= std_logic_vector(midWordTX);



	MGT_inst : component MGT port map(
		  -- Reset
        rst_txRst_i 				=> tx_reset,
        rst_rxRst_i 				=> rx_reset,
        
        -- Clocks
		  clk_mgt_reference_i 	=> clk_mgt_320MHz_tx,
        
        -- PCS Data
        data_txword_i 			=> tx_parallel_data,
        data_rxword_o 			=> rx_parallel_data,
        
        -- Serial lane
        ser_tx_o 					=> tx_serial_data,
        ser_rx_i 					=> rx_serial_data,
        
        -- Status
        sta_mgtTxRdy_o 			=> tx_ready,
        sta_mgtRxRdy_o 			=> rx_ready,
        
        -- Control
        ctr_clkSlip_i 			=> bitSlip
	  
	
	);


end behave;

