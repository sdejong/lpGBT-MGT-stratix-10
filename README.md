# lpGBT MGT prototype using Stratix 10 transceiver IP

## Introduction

   This is a prototype of the lpGBT MGT module on a Stratix 10 FPGA, using the Stratix 10 transceiver IP

## Requirement

To examine transceiver.qsys, where the transceiver IP is defined, you will need Quartus Pro 18.0, as earlier versions don't support the stratix 10. The testbench is known to work in the version of modelsim included with Quartus Pro 18.0, but may work with other versions of modelsim

## Testbench

To run the testbench, open modelsim and run

    do testbench.do

Note: after initially running, you can comment out line 24 of testbench.do (the line that says dev_com). Processing this line takes a long time.

### Testbench functions

**tx\_isReady**  -  returns 1 of tx is ready

**rx\_isReady**  -  returns 1 of rx is ready

**tx\_waitForReady** - runs the simulation until tx is ready (approx 19us)

**rx\_waitForReady** - runs the simulation until rx is ready (approx 34us)

**tx\_reset** - sends reset signal to tx portion of transceiver

**rx\_reset** - sends reset signal to rx portion of transceiver

**bitSlip** - sends the bitslip signal to the rx portion of transceiver
